
def biggest(num1, num2, num3):
    """
      >>> biggest(3, 8, 6)
      8
      >>> biggest(42, 9, 11)
      42
      >>> biggest(42, 7, 91)
      91
      >>> biggest(5, 5, 5)
      5
    """

    if num1 > num2 and num1 > num3:
        return num1
    elif num2 > num1 and num2 > num3:
        return num2
    else:
        return num3

if __name__ == '__main__':
    import doctest
    doctest.testmod()
